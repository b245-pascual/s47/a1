//console.log("JS DOM - Manipultaions");

//[Section] : Document Object Model (DOM)
	//allows us to access or modify the properties of an html element in a webpage
	//it is standard on how to get, change add or delete HTML elements 
	//we will be focusing only with DOM in terms of managing forms.

	//for selecting HTML elements we will be using document.querySelectorAll / getElementById

		//Syntax: document.querySelector("HTML element")

		//CSS selectors
			//class selector (.);
			//id selector(#);
			//tag selector(html tags);
			//universal (*);
			//attribute selector ([attribute]);

			//kung sino yung nauna sya yung matatarget
			//querySelectorAll
			let universalSelector = document.querySelectorAll("*");
			console.log(universalSelector) // naging array siya

			//querySelector
			let singleUniversalSelector = document.querySelector("*")
			console.log(singleUniversalSelector);

			let classSelector = document.querySelectorAll(".full-name");
			console.log(".full-name");

			let singleClassSelector = document.querySelector(".full-name")
			console.log(singleClassSelector)

			/*let singleClassSelector = document.querySelector("#txt-first-name")
			console.log(singleClassSelector);*/

			let tagSelector = document.querySelectorAll("input");
			console.log(tagSelector);

			//combning selector adjacent, sibling,
			let spanSelector = document.querySelector("span[id]")
			console.log(spanSelector);



			//getElement
				let element = document.getElementById("fullName");
				console.log(element);

				element = document.getElementsByClassName("fullName");
				console.log(element);



//[Section] Event Listeners
	//whenever a user interacts with a webpage, this action is considered as an event
	//working with events is large part of creating interactivity in a web page
	//specific function that will be triggered if the event happen.

	//The function we use is "addEventListener", it takes two arguments
		//first argument a string identifying the event
		// second argument, function that the listener will trigger once the "specified event" occur.

		let txtFirstName = document.querySelector("#txt-first-name");
		
		// Add event listener
		//two arguments yung string (event,function)
		txtFirstName.addEventListener("keyup", () => {
			console.log(txtFirstName.value);
			
			spanSelector.innerHTML = `${txtFirstName.value}`
			// spanSelector.innerHTML = `<h1>${txtFirstName.value}<h1>`
		})

		let txtLastName = document.querySelector("#txt-last-name");
		txtLastName.addEventListener("keyup", ()=>{
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})

		let colorSelection = document.querySelector('#text-color')


		let textColor = document.querySelector("#text-color");

		textColor.addEventListener("change", () => {
			spanSelector.innerHTML = `<font color=${textColor.value}>${txtFirstName.value} ${txtLastName.value}</font>`
		})



